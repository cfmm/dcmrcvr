FROM python:3.10.4-alpine3.16
LABEL maintainer=isolove@uwo.ca
LABEL version=3.0

RUN apk update && apk add --no-cache --update git openssh-client && rm /var/cache/apk/*

COPY dcmrcvr /opt/dcmrcvr/dcmrcvr
WORKDIR /opt/dcmrcvr

RUN pip install --no-cache-dir -r dcmrcvr/requirements.txt
RUN pip install --no-cache-dir paramiko gitpython

ENTRYPOINT python3 dcmrcvr/server.py
