import os
import shutil
from pathlib import Path

import pytest
from pydicom import Dataset
from pydicom.dataset import FileMetaDataset
from pydicom.uid import generate_uid, ImplicitVRLittleEndian

from dcmrcvr.server import start_server
from tests.dicom_client import DicomClient

from pynetdicom.sop_class import MRImageStorage

# parameters for incoming DICOM connections
calling_aet = 'CLIENT'
calling_port = 29867


@pytest.fixture(scope='module')
def data():
    return {
        'LOGLEVEL': 'CRITICAL',
        'LOGPATH': None,
        'MAIL_LOG_RECIPIENT': None,
        'MAIL_LOG_LEVEL': 'CRITICAL',
        'MAIL_HOST': None,
        'MAIL_PORT': None,
        'MAIL_SENDER': None,
        'ROOTPATH': Path(__file__).parent.joinpath('tmp'),
        'PORT': 11112,
        'AETITLE': 'DCMRCVR',
        'STALE': 60,
        'DELETE_UNMATCHED': True,
    }


@pytest.fixture(scope='module')
def server(data):

    shutil.rmtree(data['ROOTPATH'], ignore_errors=True)
    os.makedirs(data['ROOTPATH'], exist_ok=False)
    ae = start_server(data, blocking=False)
    yield ae
    ae.shutdown()
    shutil.rmtree(data['ROOTPATH'], ignore_errors=True)


@pytest.fixture(scope='module')
def plaintext_client(data):
    return DicomClient(
        host='localhost',
        port=data['PORT'],
        called_aet=data['AETITLE'],
        calling_aet=calling_aet,
        calling_port=calling_port
    )


def generate_dataset():
    ds = Dataset()
    ds.SOPInstanceUID = generate_uid()
    ds.StudyInstanceUID = generate_uid()
    ds.SeriesInstanceUID = generate_uid()
    ds.SOPClassUID = MRImageStorage
    ds.file_meta = FileMetaDataset()
    ds.file_meta.TransferSyntaxUID = ImplicitVRLittleEndian
    return ds
