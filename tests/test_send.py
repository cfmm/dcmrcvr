import socket
import time
from pynetdicom import AE


def test_echo(data, server):
    ae = AE()
    ae.add_requested_context('1.2.840.10008.1.1')
    assoc = ae.associate('localhost', data['PORT'])
    assert assoc.is_established
    assoc.release()


def test_direct(data, server):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("localhost", 11112))
    s.send(b'abcsdefghijklmnopqrstuvwxyz')
    time.sleep(0.001)
    s.close()


def test_garbage(data, server):
    ae = AE()
    ae.add_requested_context('1.2.840.10008.1.1')
    assoc = ae.associate('localhost', data['PORT'])
    assert assoc.is_established
    assoc.dul.socket.send(b'abcsdefghijklmnopqrstuvwxyz')
    assoc.dul.socket.close()
    assoc.release()
