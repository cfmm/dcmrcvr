#!/usr/bin/env python
"""
Copyright (C) 2020 Martyn Klassen

Application for configuring and running DICOM store SCP for automated processing
"""

import logging
import os
import sys

from pydicom.uid import ExplicitVRLittleEndian, ImplicitVRLittleEndian, \
    ExplicitVRBigEndian, DeflatedExplicitVRLittleEndian
from pynetdicom import AE, evt, VerificationPresentationContexts, StoragePresentationContexts

from processor import AEProcessor
from tracker import Tracker


def start_server(config, blocking=True):
    """
     Start Store SCP listen
    """

    kwargs = {'level': getattr(logging, config['LOGLEVEL']),
              'format': '%(asctime)s %(levelname)s:%(name)s:%(message)s',
              'handlers': list()}

    if config['LOGPATH']:
        kwargs['handlers'].append(logging.FileHandler(config['LOGPATH']))
    else:
        kwargs['handlers'].append(logging.StreamHandler(sys.stdout))

    if all((config['MAIL_LOG_RECIPIENT'], config['MAIL_HOST'], config['MAIL_PORT'], config['MAIL_SENDER'])):
        # Configure email error notification
        from logging.handlers import SMTPHandler

        mail_handler = SMTPHandler(
            mailhost=(config['MAIL_HOST'], int(config['MAIL_PORT'])),
            fromaddr=config['MAIL_SENDER'],
            toaddrs=[config['MAIL_LOG_RECIPIENT'], ],
            subject='[dcmrcvr] Runtime error'
        )
        mail_handler.setLevel(getattr(logging, config['MAIL_LOG_LEVEL']))
        kwargs['handlers'].append(mail_handler)

    logging.basicConfig(**kwargs)

    logging.info("Configuring server")
    if config['LOGLEVEL'] == 'DEBUG':
        # pynetdicom debug logging is much too verbose
        logging.getLogger('pynetdicom').setLevel(logging.INFO)
        logging.getLogger('watchdog').setLevel(logging.INFO)
        logging.getLogger('git').setLevel(logging.INFO)
        logging.getLogger('paramiko').setLevel(logging.INFO)

    # Port sniffers generate innumerable Unknown PDU type received errors
    # These errors need to be ignored
    logging.getLogger('pynetdicom.dul').setLevel(logging.CRITICAL)

    logging.debug(config)
    logging.debug(os.environ)

    try:
        # Set Transfer Syntax options
        transfer_syntax = [ImplicitVRLittleEndian,
                           ExplicitVRLittleEndian,
                           DeflatedExplicitVRLittleEndian,
                           ExplicitVRBigEndian]

        # Create the AE title
        ae = AE(ae_title=config['AETITLE'])

        # Configure the supported presentation contexts
        for context in VerificationPresentationContexts:
            ae.add_supported_context(context.abstract_syntax, transfer_syntax)

        # Add the required custom presentation contexts
        # SiemensCSANonImageStorageSOPClass
        ae.add_supported_context('1.3.12.2.1107.5.9.1', transfer_syntax)
        # RawDataStorageSOPClass
        ae.add_supported_context('1.2.840.10008.5.1.4.1.1.66', transfer_syntax)

        # Add default storage presentation contexts
        for context in StoragePresentationContexts:
            ae.add_supported_context(context.abstract_syntax, transfer_syntax)

        # Configure the handlers
        tracker = Tracker(
            logger=logging,
            output_dir=config.get('ROOTPATH', os.getcwd()),
            interval=int(config.get('STALE', 60)),
            delete_unmatched=config.get('DELETE_UNMATCHED', True),
        )
        processor = AEProcessor(tracker)
        handlers = [(evt.EVT_C_ECHO, processor.handle_echo),
                    (evt.EVT_C_STORE, processor.handle_store),
                    (evt.EVT_REQUESTED, processor.handle_requested),
                    (evt.EVT_ACCEPTED, processor.handle_accepted),
                    (evt.EVT_RELEASED, processor.handle_released),
                    (evt.EVT_ABORTED, processor.handle_aborted),
                    (evt.EVT_ESTABLISHED, processor.handle_established),
                    (evt.EVT_CONN_CLOSE, processor.handle_conn_close)]

        ae.dimse_timeout = config.get('DIMSE_TIMEOUT', 30)
        ae.acse_timeout = config.get('ACSE_TIMEOUT', 60)
        ae.network_timeout = config.get('NETWORK_TIMEOUT', 60)

        # Start the server
        logging.info("Starting Server")
        try:
            ae.start_server(('', int(config.get('PORT', 11112))), block=blocking, evt_handlers=handlers)
        except KeyboardInterrupt:
            # Stop all running tasks
            processor.stop()
            raise

    except Exception as e:
        logging.exception(e)
        raise

    return ae


if __name__ == '__main__':
    data = {
        'LOGLEVEL': os.getenv('DCMRCVR_LOGLEVEL', default='WARN'),
        'LOGPATH': os.getenv('DCMRCVR_LOGPATH', default=None),
        'MAIL_LOG_RECIPIENT': os.getenv('DCMRCVR_MAIL_LOG_RECIPIENT', default=None),
        'MAIL_LOG_LEVEL': os.getenv('DCMRCVR_MAIL_LOG_LEVEL', default='ERROR'),
        'MAIL_HOST': os.getenv('DCMRCVR_MAIL_HOST', default=None),
        'MAIL_PORT': os.getenv('DCMRCVR_MAIL_PORT', default=None),
        'MAIL_SENDER': os.getenv('DCMRCVR_MAIL_SENDER', default=None),
        'ROOTPATH': os.getenv('DCMRCVR_ROOTPATH', default='/tmp'),
        'PORT': os.getenv('DCMRCVR_PORT', default=11112),
        'AETITLE': os.getenv('DCMRCVR_AETITLE', default='DCMRCVR'),
        'STALE': os.getenv('DCMRCVR_STALE', default=60),
        'DELETE_UNMATCHED': bool(os.getenv('DCMRCVR_DELETE_UNMATCHED', default='1'))
    }

    start_server(data)
