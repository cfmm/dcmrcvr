"""
Copyright (C) 2020 Martyn Klassen

Classes for tracking DICOM files being received and initiating processing
"""

import hashlib
import importlib
import importlib.util
import inspect
import os
import shutil
import threading
import uuid
from datetime import datetime, timedelta
from logging import DEBUG

from pydicom import dcmread
from watchdog.events import EVENT_TYPE_CREATED, EVENT_TYPE_DELETED, EVENT_TYPE_MOVED, EVENT_TYPE_MODIFIED
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer


class TaskUpdateHandler(FileSystemEventHandler):
    """
    Handler for changes within the task directory
    """

    def __init__(self, tracker):
        """
        Initialize the handler with Tracker object.

        :param tracker: Tracker object to register changes
        """
        self.tracker = tracker
        super().__init__()

    def is_valid(self, path):
        # Only files in task_dir or task.py files in first sub-directory are tasks
        head, tail = os.path.split(path)
        return ((os.path.realpath(head) == self.tracker.task_dir and tail.lower().endswith('.py'))
                or (tail.lower()) == 'task.py' and os.path.realpath(os.path.dirname(head)) == self.tracker.task_dir)

    def on_any_event(self, event):
        # Do not care about directories, only task files
        if event.is_directory:
            return

        if self.is_valid(event.src_path):
            if event.event_type == EVENT_TYPE_CREATED or event.event_type == EVENT_TYPE_MODIFIED:
                # Creation or modification means the task needs to be loaded
                self.tracker.process_task(event.src_path, False)
            elif event.event_type == EVENT_TYPE_DELETED or event.event_type == EVENT_TYPE_MOVED:
                # Deletion or moving means the task needs to be deleted
                self.tracker.process_task(event.src_path, True)

        if event.event_type == EVENT_TYPE_MOVED and self.is_valid(event.dest_path):
            # Moving to new valid path means the task needs to loaded from the new location as a create event for the
            # new location is not trigger
            self.tracker.process_task(event.dest_path, False)


class Collector(object):
    """
    Object to track collections of Datasets for processing

    Wraps set() with a timestamp that tracks last change.
    """

    def __init__(self, assoc=None):
        # Timestamp for change tracking
        self.timestamp = datetime.now()
        # Internal set()
        self._associations = set()
        self.add(assoc)

    def add(self, element):
        # None object to not permitted in the set
        if element is None:
            return

        # Add the element (assoc reference) to set
        self._associations.add(element)
        # Record the last change time
        self.timestamp = datetime.now()

    def remove(self, element):
        # Remove the element from the set
        self._associations.remove(element)
        # Removals are not time tracked

    def inactive(self):
        """
        Check is the Collector has active associations

        :return: bool
        """
        return not self._associations


class Tracker(object):

    def __init__(self, logger, output_dir, interval=60, delete_unmatched=True):
        """

        :param logger: logging.logger for output
        :param output_dir: os.path directory for storing temporary data
        :param interval: numeric seconds of inactive for Collector to be considered complete
        :param delete_unmatched: if True, datasets which have been collected but for which no matching task was found
            at process time will be deleted.
        """
        self.logger = logger
        self.logger.debug('__init__: Start')

        self.active_runners = list()
        self.active_collectors = dict()
        self.guard = threading.Lock()
        self.stale = timedelta(seconds=interval)
        self.timer = None
        self.delete_unmatched = delete_unmatched

        # Make sure the directory for incoming data exists
        self.incoming_dir = os.path.join(output_dir, 'incoming')
        self.make_directory(self.incoming_dir)

        # Make sure the errors data exists
        self.task_errors = os.path.join(output_dir, 'errors')
        self.make_directory(self.task_errors)

        # Make sure the directory for processing data exists and is empty
        self.processing_dir = os.path.join(output_dir, 'processing')

        # Delete the processing directory and recreate - no tasks can currently be running
        try:
            shutil.rmtree(self.processing_dir)
        except FileNotFoundError:
            pass

        # Create the processing directory
        self.make_directory(self.processing_dir)

        # Dictionary of all available tasks
        self.tasks = dict()

        # Observe the task directory for changes to add and remove tasks
        self.task_dir = os.path.realpath(os.path.join(os.path.dirname(__file__), 'tasks'))
        self.base_observer = Observer()
        self.base_observer.schedule(TaskUpdateHandler(self), self.task_dir, recursive=True)
        self.base_observer.start()

        # Add all the existing tasks in the task directory
        for path in os.listdir(self.task_dir):
            if path == '__pycache__':
                continue
            path = os.path.join(self.task_dir, path)
            if os.path.isdir(path):
                path = os.path.join(path, 'task.py')

            self.process_task(path)

        # On startup the incoming tracker must be rebuilt from the existing incoming directories
        with self.guard:
            for task in os.listdir(self.incoming_dir):
                if os.path.isdir(os.path.join(self.incoming_dir, task)):
                    # Create the collector without an active association
                    self.logger.debug('__init__: new active collector for existing incoming directory {}'.format(task))
                    self.active_collectors[task] = Collector()

            # Start a time to clear out all the collectors that may have been recovered,
            # but do not have active associations
            if self.active_collectors:
                self.start_timer(self.stale.total_seconds())

    def stop(self):
        # Stop observing for new tasks
        self.base_observer.stop()
        self.base_observer.join()

        # Wait for all active tasks to complete
        for task in self.active_runners:
            task.join()

    def make_directory(self, directory):
        """
        Helper function to create directory.

        :param directory: os.path to create
        :return: None
        """
        try:
            os.makedirs(directory, mode=0o755, exist_ok=True)
        except Exception:
            self.logger.error('Unable to create the directory: {0!s}'.format(directory))
            raise

    def check_collecting(self, name, assoc, check):
        """
        Add association to collector if required

        :param name: str name of the collector
        :param assoc: pynetdicom.Association
        :param check: callable to determine if collection is required
        :return: bool
        """
        self.logger.debug('check_collecting: init')
        # Changing the active_collectors must to restricted to a single thread
        with self.guard:
            # Check if the there is already a collector for the series
            if name in self.active_collectors:
                self.logger.debug(
                    'check_collecting: add the current association {} to the active collector {}'.format(assoc, name))
                # Add the current association to the collector
                self.active_collectors[name].add(assoc)
                return True
            elif check():
                self.logger.debug(
                    'check_collecting: create new active collector {} for the current association {}'.format(name,
                                                                                                             assoc))
                # Create a new task entry and add the assoc to it
                self.active_collectors[name] = Collector(assoc)
                return True

        return False

    def _gen_tasks(self, dataset, study):
        for task in self.tasks.values():
            # noinspection PyBroadException
            try:
                if task.study == study and task.match(dataset):
                    yield task
            except Exception as err:
                self.logger.debug("Error in task match")
                self.logger.exception(err)

    def has_task(self, dataset, study=False):
        try:
            task = next(self._gen_tasks(dataset, study))
            self.logger.debug(f"has_task: matched dataset {dataset.SOPInstanceUID} with {task}")
            return True
        except StopIteration:
            return False

    def get_tasks(self, dataset, study=False):
        return list(self._gen_tasks(dataset, study))

    def destinations(self, event):
        """

        :param event: pynetdicom.event.event The event corresponding to a C-STORE request.
        :return: list of filenames to which the dataset should be saved. All subdirectories are created.
        """

        # Because pydicom uses deferred reads for its decoding, decoding errors
        #   are hidden until encountered by accessing a faulty element

        assoc = event.assoc
        dataset = event.dataset

        instance_uid = dataset.SOPInstanceUID
        study_uid = dataset.StudyInstanceUID
        series_uid = dataset.SeriesInstanceUID

        # Record the source since the same data from separate sources should be processed separately
        source = '{0}_{1}'.format(assoc.requestor.ae_title.strip(), assoc.requestor.address)

        # Create a list of locations to which to save the instance
        locations = list()

        # Check if study is to be processed
        task_name = '{0}_{1}'.format(source, study_uid)
        if self.check_collecting(task_name, assoc, lambda: self.has_task(dataset, study=True)):
            # Save instance to series directory in task directory
            filename = os.path.join(self.incoming_dir, task_name, series_uid)
            self.make_directory(filename)
            filename = os.path.join(filename, instance_uid)
            locations.append(filename)

        # Check if series is to be processed (separate from the study)
        task_name = '{0}_{1}_{2}'.format(source, study_uid, series_uid)
        if self.check_collecting(task_name, assoc, lambda: self.has_task(dataset, study=False)):
            # Save instance to task directory
            filename = os.path.join(self.incoming_dir, task_name)
            self.make_directory(filename)
            filename = os.path.join(filename, instance_uid)
            locations.append(filename)

        # Return the list of all locations to which the dataset should be saved
        return locations

    def start_timer(self, interval):
        """
        :param interval: interval in seconds after which to run callback
        :return:
        """
        if self.timer is None and interval > 0:
            self.timer = threading.Timer(interval, self.process)
            self.logger.debug(f"start_timer: starting process timer (execute in {interval} s)")
            self.timer.start()

    def remove(self, assoc):
        """
        Remove an Association from collection tracking. Should be called when Association terminates.

        :param assoc: pynetdicom.Association which has closed
        :return: None
        """
        # Nuisance non-DICOM connection will not have an association, so they neither need to be removed nor trigger
        # a timer.
        if assoc is None:
            return

        with self.guard:
            tracked = False
            for item in self.active_collectors.values():
                self.logger.debug(f"remove: removing association {assoc} from active collector {item}")
                try:
                    item.remove(assoc)
                    tracked = True
                except KeyError:
                    continue

            if tracked:
                # Set a timer to run the processing in stale seconds when the association was being tracked
                self.start_timer(self.stale.total_seconds())

    def process_task(self, path, remove=False):
        """

        :param path: os.path of the python module containing the task
        :param remove: bool flag to remove the Task instead of adding
        :return:
        """

        # Changing tasks should happen one thread at a time
        with self.guard:
            if remove:
                try:
                    # Remove the task from further processing
                    self.tasks.pop(path)
                    self.logger.info(f'Removed task {path}')
                except KeyError:
                    # Already did not exist
                    pass
            else:
                # The path must exist and be a file
                if not os.path.isfile(path):
                    self.logger.error(f"Unable to load task from {path}: Not a file")
                    return

                # noinspection PyBroadException
                try:
                    task_module = None

                    # Check if a task already exists, i.e. change to underlying file
                    if path in self.tasks:
                        # Reload the module corresponding to the existing task
                        task_module = inspect.getmodule(self.tasks[path])
                        if task_module:
                            if task_module.__file__ == path:
                                importlib.reload(task_module)
                                self.logger.info(f"Successfully reloaded task from {path}")
                            else:
                                self.logger.error(f"Task path {path} does not match module file {task_module.__file__}")
                                task_module = None
                        else:
                            self.logger.error(f"Unable to get module for {path}")

                    if not task_module:
                        # Load the path as a task module
                        # Use the md5 hash of the path as the name
                        name = hashlib.md5(path.encode('utf-8'))
                        spec = importlib.util.spec_from_file_location(f"dcmrcvr.task.{name}", path)
                        task_module = importlib.util.module_from_spec(spec)
                        # noinspection PyUnresolvedReferences
                        spec.loader.exec_module(task_module)

                    # Create the Task from the module
                    # noinspection PyUnresolvedReferences
                    self.tasks[path] = task_module.Task(source=path)
                    self.logger.info(f"Created task {path}")

                except Exception as exc:
                    # Malformed task or error loading the task
                    self.logger.error("Unable to load task from {0!s}".format(path))
                    self.logger.exception(exc)
                    pass

    def process(self):
        """
        Check for completed collectors and run appropriate tasks

        :return: None
        """
        self.logger.debug('process: active runners: {}; active collectors: {}'.format(len(self.active_runners),
                                                                                      len(self.active_collectors)))
        with self.guard:
            self.timer = None
            # Remove any completed runners
            self.active_runners = [x for x in self.active_runners if x.is_alive()]

            remove = list()
            next_process = None

            for key, value in self.active_collectors.items():
                self.logger.debug('process: processing collector {}'.format(key))
                if value.inactive():
                    expire_on = value.timestamp + self.stale
                    if expire_on < datetime.now():
                        # Collector is ready for processing
                        self.logger.debug('process: active collector {} ready for processing.'.format(key))

                        # Move data from incoming to processing
                        source = os.path.join(self.incoming_dir, key)
                        # Add a UUID in case multiple dataset are simultaneously being processed
                        destination = os.path.join(self.processing_dir, uuid.uuid4().__str__(), key)
                        shutil.move(source, destination)

                        # Remove task from active_datasets
                        self.logger.debug('process: putting active collector {} on remove list.'.format(key))
                        remove.append(key)

                        # Create a thread for each collection, because they potentially have a lot of IO waiting
                        self.logger.debug('process: starting runner for active collector {}.'.format(key))
                        runner = threading.Thread(target=lambda: self.run_tasks(destination))
                        runner.start()
                        self.logger.debug(
                            'process: putting runner for collector {} on active runners list.'.format(key))
                        self.active_runners.append(runner)
                    else:
                        next_process = min(next_process, expire_on) if next_process else expire_on

            for key in remove:
                self.logger.debug('process: removing active collector {}.'.format(key))
                self.active_collectors.pop(key)

            # Start another
            if next_process:
                wait_time = next_process - datetime.now()
                self.start_timer(max(wait_time.total_seconds(), 0.1))

    def run_tasks(self, source):
        # Series will have instances in the top directory of source, so start by checking for series processing
        study = False

        # Tasks to run on the collection of data
        tasks = list()

        self.logger.debug('run_tasks: init')

        # noinspection PyShadowingNames
        def check_files(root, filenames, study):
            for filename in filenames:
                try:
                    # Get a list of all the tasks that should be run on the collection of DICOM Datasets
                    return self.get_tasks(dcmread(os.path.join(root, filename)), study)
                except EOFError:
                    # Not a valid DICOM file, so try the next file
                    continue

            # No files matched any tasks (Studies will have no files in the root directory)
            return None

        task_set = set()
        root, directories, filenames = next(os.walk(source, topdown=True));

        # Check the files for series tasks
        tasks = check_files(root, filenames, False)

        if tasks is None:
            # No files in the destination root were DICOM files, so need to check for study tasks
            # All the series in the study need to be checked for match to any tasks
            for directory in directories:
                series_root, _, filenames = next(os.walk(os.path.join(root, directory)))
                tasks = check_files(series_root, filenames, True)
                if tasks is not None:
                    task_set.update(tasks)
        else:
            task_set.update(tasks)

        # source is in folder with uuid, get this base folder name
        base = os.path.dirname(source)

        if not task_set:
            # If no tasks were found, it's possible that the task which matched the dataset has been changed while data
            # was being collected, and its `match` method no longer matches the dataset.
            #
            # It's also possible that something went wrong. Report as a warning.
            self.logger.warning(f'Failed to find a task for collected dataset in {source}.')

            if not self.delete_unmatched:
                self.logger.critical(f'Keeping collected dataset {source}')
                shutil.move(base, self.task_errors)
                return

        # Run the tasks on the data and then delete the data
        for task in task_set:
            # noinspection PyBroadException
            try:
                if self.logger.getLogger(__name__).isEnabledFor(DEBUG):
                    self.logger.debug(f"run_tasks: running {task} on {source}")
                task.process(source)
            except Exception:
                self.logger.error(f"Failure to process {task} on {source}", exc_info=True)
                shutil.move(base, self.task_errors)
                return

        # Delete the data after processing
        self.logger.debug(f"run_task: removing {base}")
        shutil.rmtree(base, ignore_errors=True)
