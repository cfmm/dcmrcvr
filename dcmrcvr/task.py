"""
Copyright (C) 2020 Martyn Klassen

Base class from which all Tasks should inherit
"""
import logging

import yaml
import pathlib


class Task(object):
    def __init__(self, source, config=None):
        self.path = source
        self.logger = logging.getLogger(str(self))
        self.config = config or dict()
        config_path = pathlib.Path(self.path).parent.joinpath("config.yml")
        if config_path.is_file():
            self.logger.debug(f"Loading task config from {config_path}")
            with open(config_path, 'r') as fp:
                self.config.update(yaml.safe_load(fp))

    def __str__(self):
        return f"Task from {self.path}"

    @property
    def study(self):
        """
        Property indicating whether task uses a study, i.e. all series in a study

        Tasks either require all series in a study or a single series. Tasks cannot require a subset of series. The
        task can ignore series it does not require during processing, but all series of a study will be collected for
        study tasks.

        :return: bool
        """
        return False

    def match(self, dataset):
        """
        Check if series of dataset is required for task

        :param dataset: pydicom.Dataset whose series is to be tested
        :return: bool
        """
        raise NotImplementedError

    def process(self, source):
        """
        Implementation of the task processing. The task shall never change the contents of source. The task should
        generally use another process for execution. The data will be deleted when the task returns, so an external
        copy must be made if the process requires continued access after starting.

        This will often be submitting a job to cloud computing infrastructure.

        :param source: os.path containing DICOM files for processing
        :return: None
        """
        raise NotImplementedError
