from .conftest import generate_dataset


def test_echo(server, plaintext_client):
    response = plaintext_client.echo()

    assert response is not None and response.Status == 0x0000


def test_store(server, plaintext_client):

    dataset = generate_dataset()
    response = plaintext_client.store(dataset)

    assert response[0] is not None and response[0].Status == 0x0000
