import task


class Task(task.Task):
    @property
    def study(self):
        # Task requires all series in study
        return True

    def match(self, dataset):
        # Task is never run
        return False

    def process(self, source):
        # Nothing is done
        pass
