import time

import pydicom
from pydicom import read_file

from pynetdicom import AE, StoragePresentationContexts, VerificationPresentationContexts


class DicomClient(object):
    def __init__(self, host, port, called_aet='DCMRCVR', calling_aet='CLIENT', calling_port=29867,
                 tls_client_context=None):
        self.host = host
        self.port = port
        self.called_aet = called_aet
        self.calling_aet = calling_aet
        self.calling_port = calling_port
        self.tls_client_context = tls_client_context

    def establish_association(self, retry=False, requested_contexts=None):
        # ensure the DICOM server is up and ready to establish associations
        assoc = self.attempt_negotiation(requested_contexts)
        if retry:
            while not assoc.is_established:
                time.sleep(0.5)
                assoc = self.attempt_negotiation(requested_contexts)
        return assoc

    def attempt_negotiation(self, requested_contexts=None):
        ae = AE(self.calling_aet)
        ae.requested_contexts = requested_contexts

        return ae.associate(self.host, self.port, ae_title=self.called_aet)

    def echo(self):
        assoc = self.establish_association(requested_contexts=VerificationPresentationContexts)

        if assoc.is_established:
            status = assoc.send_c_echo()

            assoc.release()

            return status

        return None

    def store(self, ds_or_path):
        assoc = self.establish_association(requested_contexts=StoragePresentationContexts)

        if assoc.is_established:
            if isinstance(ds_or_path, str):
                ds = read_file(ds_or_path)
            elif isinstance(ds_or_path, pydicom.Dataset):
                ds = ds_or_path
            else:
                raise RuntimeError('ds_or_path needs to be a python path or a pydicom Dataset')

            # `status` is the response from the peer to the store request
            # but may be an empty pydicom Dataset if the peer timed out or
            # sent an invalid dataset.
            status = assoc.send_c_store(ds)

            assoc.release()

            return status, ds

        return None, None
