"""
Copyright (C) 2020 Martyn Klassen

Class for processing DICOM events
"""

import os

from pydicom.filewriter import write_file_meta_info


class AEProcessor(object):

    def __init__(self, tracker):
        """

        :param tracker: Tracker object for determining storage locations
        """
        self.logger = tracker.logger

        # Tracker maintains information about associations
        self.tracker = tracker

    def stop(self):
        """
        Stop all threads that were started.

        :return:
        """

        # Stop any running tracker jobs
        self.tracker.stop()

    def log(self, msg, event):
        # Every *Event* includes `assoc` and `timestamp` attributes
        #   which are the *Association* instance the event occurred in
        #   and the *datetime.datetime* the event occurred at
        requestor = event.assoc.requestor
        timestamp = event.timestamp.strftime("%Y-%m-%d %H:%M:%S")
        self.logger.info('{} ({}, {}): {}'.format(timestamp, requestor.address, requestor.port, msg))

    def handle_store(self, event):
        """Handle a C-STORE request.
        Parameters
        ----------
        event : pynetdicom.event.event
            The event corresponding to a C-STORE request.
        Returns
        -------
        status : pynetdicom.sop_class.Status or int
            A valid return status code, see PS3.4 Annex B.2.3 or the
            ``StorageServiceClass`` implementation for the available statuses
        """
        self.log("Received C-STORE of {} in {}".format(event.context.abstract_syntax, event.context.transfer_syntax),
                 event)

        # noinspection PyBroadException
        try:
            destinations = self.tracker.destinations(event)
        except OSError:
            self.logger.error("OSError determining destinations")
            # Failed - Out of Resources - IOError
            return 0xA700
        except Exception as exc:
            self.logger.error("Unable to decode dataset")
            self.logger.exception(exc)
            # Unable to decode dataset
            return 0xC210

        # Write the DICOM file to all required destinations
        for filename in destinations:
            self.logger.info('Storing DICOM file: {0!s}'.format(filename))

            if os.path.exists(filename):
                self.logger.warning('DICOM file {0!s} already exists, overwriting'.format(filename))

            try:
                # Write the raw data as received
                with open(filename, 'wb') as f:
                    # Write the preamble and prefix
                    f.write(b'\x00' * 128)
                    f.write(b'DICM')
                    # Encode and write the File Meta Information
                    write_file_meta_info(f, event.file_meta)
                    # Write the encoded dataset
                    f.write(event.request.DataSet.getvalue())
            except IOError:
                self.logger.error('Could not write file (Check permissions): {0!s}'.format(filename))
                # Failed - Out of Resources - IOError
                return 0xA700
            except Exception as exc:
                self.logger.error('Could not write file: {0!s}'.format(filename))
                self.logger.exception(exc)
                # Failed - Out of Resources - Miscellaneous error
                return 0xA701

        # Success
        return 0x0000

    def handle_accepted(self, event):
        self.log("Accepted A-ASSOCIATE", event)

    def handle_requested(self, event):
        self.log("Received A-ASSOCIATE-RQ", event)

    def handle_released(self, event):
        self.log('Released A-ASSOCIATE', event)

    def handle_established(self, event):
        self.log('Established A-ASSOCIATE', event)

    def handle_aborted(self, event):
        self.log('Aborted A-ASSOCIATE', event)

    def handle_conn_close(self, event):
        self.log('Connection closed', event)

        # When a connection closes, for any reason, the association is finished and should be removed from the tracker
        self.tracker.remove(event.assoc)

    def handle_echo(self, event):
        """Handle a C-ECHO service request.

        Parameters
        ----------
        event : evt.Event
            The C-ECHO service request event.

        Returns
        -------
        int or pydicom.dataset.Dataset
            The status returned to the peer AE in the C-ECHO response.
            Must be a valid C-ECHO status value as either an ``int`` or a
            ``Dataset`` object containing an (0000,0900) *Status* element.
        """
        self.log("Received C-ECHO service request", event)

        # Return a *Success* status
        return 0x0000
