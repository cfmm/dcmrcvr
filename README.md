dcmrcvr
=======

dcmrcvr docker image that runs a DICOM Store SCP that automatically runs tasks on received studies and series.

Environment Variables
---------------------

DCMRCVR_ROOTPATH
: Path for temporary storage of DICOM files during collection and processing [/data]

DCMRCVR_AETITLE
: AE Title of the Store SCP [DCMRCVR]

DCMRCVR_PORT
: Listening port [11116]

DCMRCVR_LOGLEVEL
: Logging level [WARN]

DCMRCVR_STALE
: Time in seconds after last received DICOM image at which point dataset is considered complete and ready for processing
[60]

Tasks Definition
----------------
Tasks are defined as python modules with a Task class that defines:
- `study` property which is True of tasks that require multiple series from a study
- `match(dataset)` method which checks if `pydicom.Dataset` is required by the task
- `process(path)` method which executes the task processing on DICOM objects contained in `path`

Tasks should generally:
- never change any files within `path`
- run long running processing in a separate process
- not access DICOM objects after returning from process method

Tasks modules should be located in `/opt/dcmrcvr/tasks`

Running
-------
Tasks should be bind mounted into the container and the DICOM port should be published.

```shell
docker run -v `pwd`/tasks:/opt/dcmrcvr/tasks  -p 127.0.0.1:11116:11116/tcp --name test -it dcmrcvr
```

If you bind mount `DCMRVR_ROOTPATH`, do not run multiple instance with the same output `DCMRVR_ROOTPATH`.
